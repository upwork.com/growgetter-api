from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title="Growgetter Watermarker API",
        default_version='v1',
        description="Watermarks Growgetter Comics and emails them to Patrons!",
        terms_of_service="https://www.growgettercomics.com",
        contact=openapi.Contact(email="dev@xareer.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
)
urlpatterns = [
    path(
        r'api/',
        schema_view.with_ui('redoc', cache_timeout=0),
        name='schema-redoc',
    ),
]
