import uuid

from django.db import models
from django.utils import timezone

# Create your models here.


class Campaign(models.Model):
    created = models.DateTimeField(default=timezone.now)
    description = models.CharField(max_length=255, null=True, unique=True)
    email_template_id = models.IntegerField(null=True)
    modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255, unique=True, null=False)
    owner = models.CharField(max_length=255, null=False)
    doc_id = models.CharField(max_length=20, null=True)

    def __str__(self):
        return self.name


class CampaignLog(models.Model):
    campaign = models.ForeignKey(
        'Campaign',
        null=False,
        on_delete=models.CASCADE,
    )
    ended_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255, unique=False, null=False)
    started_at = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=255, unique=False, null=False)
    run_id = models.UUIDField(default=uuid.uuid4, unique=True)

    def __str__(self):
        return self.name


class Target(models.Model):
    name = models.CharField(max_length=255, null=False)
    email = models.CharField(max_length=255, unique=False, null=False)
    campaign_id = models.ForeignKey(
        Campaign, null=False, unique=False, on_delete=models.CASCADE
    )
    added = models.DateTimeField(default=timezone.now)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=('campaign_id', 'email'),
                name='api_target_email_campaign_id',
            )
        ]

    def __str__(self):
        return self.name


class TargetLog(models.Model):
    campaign = models.ForeignKey(
        Campaign,
        null=False,
        on_delete=models.CASCADE,
    )
    created = models.DateTimeField(default=timezone.now)
    email = models.CharField(max_length=255, null=False)
    fileurl = models.CharField(max_length=255, unique=True, null=True)
    tracking_url = models.CharField(max_length=255, unique=True, null=True)
    run_id = models.UUIDField(null=False, unique=False)

    def __str__(self):
        return self.email


class EmailTemplate(models.Model):
    name = models.CharField(max_length=255, unique=True, null=False)
    description = models.CharField(max_length=255, null=True)
    created = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)
    body = models.TextField(null=False)
    subject = models.CharField(max_length=255, unique=True, null=False)

    def __str__(self):
        return self.name


class Comic(models.Model):
    name = models.CharField(max_length=255, unique=True, null=False)
    doc_id = models.CharField(max_length=20, unique=True, null=False)
    created = models.DateTimeField(default=timezone.now)
    docpath = models.CharField(max_length=255, unique=True, null=False)

    def __str__(self):
        return self.name
