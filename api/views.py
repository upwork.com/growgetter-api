import secrets

from django.core.files.storage import FileSystemStorage
from django.db import utils
from django.forms.models import model_to_dict
from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.exceptions import APIException
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import IsAuthenticated

from api.signal_handler import CampaignStartedException
from growgetter.settings import COMIC_DIR
from watermark.tasks import task_watermark_upload_email

from .models import Campaign, CampaignLog, Comic, EmailTemplate, Target, TargetLog
from .serializers import (
    CampaignLogSerializer,
    CampaignSerializer,
    EmailTemplateSerializer,
    FileUploadSerializer,
    TargetLogSerializer,
    TargetSerializer,
)


class CampaignView(viewsets.ModelViewSet):
    '''Create a campaign'''

    authentication_classes = [TokenAuthentication]
    persmission_classes = [IsAuthenticated]
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer

    @action(detail=True, methods=['get'])
    def start(self, request, pk=None):
        '''Retrieve all targets from DB, and enqueue'''
        campaign = Campaign.objects.get(pk=pk)
        doc = Comic.objects.get(pk=campaign.doc_id)
        targets = Target.objects.filter(campaign_id=campaign.id)
        template = EmailTemplate.objects.get(pk=campaign.email_template_id)

        try:
            campaignlog = CampaignLog.objects.create(
                campaign_id=campaign.id, name=campaign.name, status='STARTED'
            )
        except utils.IntegrityError:
            raise CampaignStartedException()

        data = {
            'campaign': campaign,
            'doc': doc,
            'targets': targets,
            'template': template,
            'campaignlog': campaignlog,
        }
        task_watermark_upload_email(data=data)(blocking=False)

        return JsonResponse(model_to_dict(campaignlog))

    @action(detail=True, methods=['get'])
    def stop(self, request, pk=None):
        ...

    def patch(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class CampaignLogView(viewsets.ReadOnlyModelViewSet):
    authentication_classes = [TokenAuthentication]
    persmission_classes = [IsAuthenticated]
    queryset = CampaignLog.objects.all()
    serializer_class = CampaignLogSerializer


class TargetView(viewsets.ModelViewSet):
    authentication_classes = [TokenAuthentication]
    persmission_classes = [IsAuthenticated]
    queryset = Target.objects.all()
    serializer_class = TargetSerializer


class TargetLogView(viewsets.ReadOnlyModelViewSet):
    authentication_classes = [TokenAuthentication]
    persmission_classes = [IsAuthenticated]
    queryset = TargetLog.objects.all()
    serializer_class = TargetLogSerializer


class EmailTemplateView(viewsets.ModelViewSet):
    authentication_classes = [TokenAuthentication]
    persmission_classes = [IsAuthenticated]
    queryset = EmailTemplate.objects.all()
    serializer_class = EmailTemplateSerializer


class FileUploadView(viewsets.ModelViewSet):
    authentication_classes = [TokenAuthentication]
    persmission_classes = [IsAuthenticated]
    queryset = Comic.objects.all()
    parser_classes = [FileUploadParser]
    serializer_class = FileUploadSerializer

    def put(self, request):
        file_obj = request.data['file']
        fs = FileSystemStorage(location=COMIC_DIR)
        fs.save(file_obj, file_obj)
        c = Comic(
            name=file_obj, doc_id=secrets.token_hex(4), docpath=f'{COMIC_DIR}{file_obj}'
        )

        try:
            c.save()
        except utils.IntegrityError as e:
            raise APIException(e)

        return JsonResponse({'name': str(c.name), 'id': c.id, 'created': c.created})
