from django.core.validators import validate_email
from rest_framework.serializers import ModelSerializer

from .models import (Campaign, CampaignLog, Comic, EmailTemplate, Target,
                     TargetLog)


class CampaignSerializer(ModelSerializer):
    class Meta:
        model = Campaign
        exclude = ()


class CampaignLogSerializer(ModelSerializer):
    class Meta:
        model = CampaignLog
        exclude = ()


class TargetSerializer(ModelSerializer):
    class Meta:
        model = Target
        exclude = ()

    def validate_email(self, value):
        if not validate_email(value):
            return value


class TargetLogSerializer(ModelSerializer):
    class Meta:
        model = TargetLog
        exclude = ()


class EmailTemplateSerializer(ModelSerializer):
    class Meta:
        model = EmailTemplate
        exclude = ()


class FileUploadSerializer(ModelSerializer):
    class Meta:
        model = Comic
        exclude = ()
