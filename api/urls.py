from django.urls import include, path
from rest_framework import routers

from .views import (
    CampaignLogView,
    CampaignView,
    EmailTemplateView,
    FileUploadView,
    TargetLogView,
    TargetView,
)

router = routers.SimpleRouter()
router.register(r'campaigns', CampaignView)
router.register(r'campaignlogs', CampaignLogView)
router.register(r'targets', TargetView)
router.register(r'targetlogs', TargetLogView)
router.register(r'emailtemplates', EmailTemplateView)
router.register(r'files', FileUploadView, basename='Comic')

urlpatterns = [
    path('', include(router.urls)),
]
