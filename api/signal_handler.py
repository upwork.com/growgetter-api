from rest_framework.exceptions import APIException


class CampaignStartedException(APIException):
    status_code = 202
    default_detail = 'Your campaign is running! See /api/campaignlogs/'
    default_code = 'campaign_started'
