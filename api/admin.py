from django.contrib import admin

# Register your models here.
from api.models import Campaign, CampaignLog

admin.site.register(Campaign)
admin.site.register(CampaignLog)
