from string import Template

import requests

from growgetter.settings import (MAILGUN_APIKEY, MAILGUN_DOMAIN, MAILGUN_EMAIL,
                                 MAILGUN_SENDER)


def send_mail(target, downloadlink: str, template) -> str:
    def sub_message_body():
        return Template(template.body).substitute(
            fullname=target.name,
            downloadlink=downloadlink,
        )

    auth = ('api', MAILGUN_APIKEY)
    data = {
        'from': f'{MAILGUN_SENDER} <{MAILGUN_EMAIL}>',
        'to': target.email,
        'subject': Template(template.subject).substitute(fullname=target.name),
        'html': sub_message_body(),
        'text': sub_message_body(),
    }

    return requests.post(
        f'https://api.mailgun.net/v3/{MAILGUN_DOMAIN}',
        auth=auth,
        data=data,
    ).json()
