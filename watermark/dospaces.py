import datetime
from typing import Optional

import boto3
import mmh3

from growgetter.settings import (SPACES_BUCKET, SPACES_ENDPOINT_URL,
                                 SPACES_KEY, SPACES_REGION, SPACES_SECRET)


def S3_session():
    session = boto3.session.Session()
    return session.client(
        's3',
        region_name=SPACES_REGION,
        endpoint_url=SPACES_ENDPOINT_URL,
        aws_access_key_id=SPACES_KEY,
        aws_secret_access_key=SPACES_SECRET,
    )


def doc_name(email: str) -> str:
    date = datetime.datetime.today().strftime('%Y%m%d')
    hashedemail = mmh3.hash(email, signed=False)
    return f'{date}_{hashedemail}_growgettercomics.pdf'


def S3_upload(watermarked_doc_path: str, email: str) -> Optional[str]:
    session = S3_session()
    key = doc_name(email)

    session.upload_file(
        watermarked_doc_path,
        Key=key,
        Bucket=SPACES_BUCKET,
        ExtraArgs={
            'ACL': 'public-read',
            'ContentType': 'application/pdf',
            'CacheControl': '14',
        },
    )

    metadata = session.head_object(
        Bucket=SPACES_BUCKET,
        Key=key,
    )

    if metadata['ResponseMetadata']['HTTPStatusCode'] == 200:
        return session.generate_presigned_url(
            'get_object', {'Bucket': SPACES_BUCKET, 'Key': key}
        ).split('?')[0]
