import io

from PyPDF2 import PdfFileReader, PdfFileWriter
from reportlab.lib.colors import black
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas

from growgetter.settings import FONT_DIR, WATERMARKED_DOC_PATH

pdfmetrics.registerFont(TTFont('DEFAULT_FONT', FONT_DIR / 'roboto.ttf'))


def doc_as_binary(docpath: str):
    return open(docpath, 'rb')


def watermark(name: str, docpath: str):
    doc = doc_as_binary(docpath)
    source_pdf = PdfFileReader(doc)
    packet = io.BytesIO()

    can = canvas.Canvas(packet)
    can.setFillColor(black)
    can.setFont('DEFAULT_FONT', 10)
    can.drawString(50, 15, name)
    can.save()

    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    output = PdfFileWriter()

    for i in range(source_pdf.getNumPages()):
        page = source_pdf.getPage(i)
        page.mergePage(new_pdf.getPage(0))
        output.addPage(page)

    with open(WATERMARKED_DOC_PATH, "wb") as outputStream:
        output.write(outputStream)

    doc.close()
