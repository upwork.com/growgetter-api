from huey.contrib.djhuey import db_task

from api.models import TargetLog
from growgetter.settings import WATERMARKED_DOC_PATH

from .dospaces import S3_upload
from .mailgun import send_mail
from .watermark import watermark


@db_task(expires=7)
def task_watermark_upload_email(data: dict):
    def process():
        for target in data['targets']:
            watermark(name=target.name, docpath=data['doc'].docpath)
            presigned_url = S3_upload(
                watermarked_doc_path=str(WATERMARKED_DOC_PATH),
                email=target.email,
            )
            send_mail(
                target=target,
                downloadlink=presigned_url,
                template=data['template'],
            )
            TargetLog.objects.create(
                campaign_id=data['campaign'].id,
                email=target.email,
                fileurl=presigned_url,
                run_id=data['campaignlog'].run_id,
            )

    if len(data['targets']) > 0:
        process()

    data['campaignlog'].status = 'COMPLETED'
    data['campaignlog'].save()
